FROM microsoft/dotnet:2.2-aspnetcore-runtime
MAINTAINER br.com.beblue
RUN apt-get update 
RUN apt-get install -y jq curl
RUN mkdir -p /app

ENV CORECLR_ENABLE_PROFILING="1" \
	CORECLR_PROFILER="{36032161-FFC0-4B61-B559-F6C5D41BAE5A}" \
	CORECLR_NEWRELIC_HOME="/usr/local/newrelic-netcore20-agent" \
	CORECLR_PROFILER_PATH="/usr/local/newrelic-netcore20-agent/libNewRelicProfiler.so" \
	NEW_RELIC_LICENSE_KEY="81273e479acd9fd9a6f6e5f70af5c8197293299c" \
	NEW_RELIC_APP_NAME="DotNetBoilerplate"

WORKDIR /app
COPY src/DotNetBoilerplate/publish .

ARG NewRelic=./newrelic
COPY $NewRelic ./newrelic

RUN dpkg -i ./newrelic/newrelic-netcore20-agent*.deb

ENTRYPOINT exec dotnet DotNetBoilerplate.dll
CMD [""]