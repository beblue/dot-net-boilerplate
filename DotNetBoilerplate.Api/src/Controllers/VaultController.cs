using Beblue.Infra.Vault;
using Microsoft.AspNetCore.Mvc;

namespace DotNetBoilerplate.Api.Controllers
{
    [ApiController]
    public class VaultController : Controller
    {
        [Route("vault")]
        public IActionResult Index()
        {
            var credential = VaultService.Instance.GetDatabaseCredentials();
            return Ok(credential);
        }
    }
}