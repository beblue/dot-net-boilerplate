﻿namespace DotNetBoilerplate.Infra.Storage
{
    public class StorageConfiguration
    {
        public string BucketName { get; private set; }
        public string Path { get; private set; }

        public StorageConfiguration(string bucketName, string path)
        {
            BucketName = bucketName;
            Path = path;
        }
    }
}
