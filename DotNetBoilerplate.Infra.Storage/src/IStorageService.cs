﻿using System.IO;

namespace DotNetBoilerplate.Infra.Storage
{
    public interface IStorageService
    {
        string Upload(Stream fileStream, string fileName, StorageConfiguration configuration);
    }
}
