﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using System;
using System.Linq;
using System.Net;
using Beblue.Infra.Logging.Extensions;
using Beblue.Infra.Logging.Models;
using Beblue.Infra.Vault.Extensions;
using Beblue.Infra.Vault.Models;

namespace DotNetBoilerplate.Api
{
    public static class Program
    {
        public static void Main(string[] args)
        {
            var configuration = GetConfiguration();
            CreateWebHostBuilder(args, configuration).Build().Run();
        }

        private static IWebHostBuilder CreateWebHostBuilder(string[] args, IConfigurationRoot configuration) =>
            WebHost.CreateDefaultBuilder(args)
                .UseKestrel(x => x.Listen(IPAddress.Any,
                    new Random().Next(1024, 65535)))
                .UseStartup<Startup>()
                .UseBeblueLogging(LogLevel.Debug)
                .UseVault(CreateVaultConfiguration(configuration));

        private static VaultConfiguration CreateVaultConfiguration(IConfigurationRoot configuration) => new VaultConfiguration()
        {
            UseDatabase = Convert.ToBoolean(configuration["VaultConfig:UseDatabase"]),
            RoleId = configuration["VaultConfig:RoleId"],
            Url = configuration["VaultConfig:Url"],
            SecretId = configuration["VaultConfig:SecretId"],
            DatabasesRolesNames = configuration.GetSection("VaultConfig:DatabasesRolesNames").AsEnumerable()
                .Where(x => !string.IsNullOrEmpty(x.Value)).Select(x => x.Value).ToList(),
            UseRabbitMq = Convert.ToBoolean(configuration["VaultConfig:UseRabbitMq"]),
            RabbitMqRoleName = configuration["VaultConfig:RabbitMqRoleName"]
        };

        private static IConfigurationRoot GetConfiguration()
        {
            var webHostBuilder = new WebHostBuilder();
            var environment = webHostBuilder.GetSetting("environment");

            var builder = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{environment}.json", optional: true)
                .AddEnvironmentVariables();

            return builder.Build();
        }
    }
}
