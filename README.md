# ![Beblue](docs/beblue.png) BEBLUE **DOT.NET** boilerplate


[![](https://img.shields.io/bitbucket/pipelines/henriquepolsani/dot-net-boilerplate/:branch.svg)](https://bitbucket.org/henriquepolsani/dot-net-boilerplate/addon/pipelines/home)
[![codecov](https://codecov.io/bb/henriquepolsani/dot-net-boilerplate/branch/master/graph/badge.svg)](https://codecov.io/bb/henriquepolsani/dot-net-boilerplate)
[![Codacy Badge](https://api.codacy.com/project/badge/Grade/94ec30f8796b43b89ace329554d3d94a)](https://www.codacy.com/app/henrique_polsani/dot-net-boilerplate?utm_source=henriquepolsani@bitbucket.org&amp;utm_medium=referral&amp;utm_content=henriquepolsani/dot-net-boilerplate&amp;utm_campaign=Badge_Grade)

## What is .NET Boilerplate

It's a project that provides the setup of a new .NET Core *micro-service*  and variety of tools. One of the goals, was to create an AWS cloud ready solution, that integrate with ECS, S3 and others AWS services.
 
### Some of project tools

* [RESTful API](https://www.restapitutorial.com/) implementation with [ASP.NET Core](https://docs.microsoft.com/en-us/aspnet/core/?view=aspnetcore-2.2)
* [Domain Driven Design](http://dddcommunity.org/) fundamentals
* [Test](https://martinfowler.com/bliki/TestDrivenDevelopment.html)/[Behavior](https://martinfowler.com/bliki/GivenWhenThen.html) Driven Development
* Application testing with [XUnit](https://github.com/xunit/xunit), mocking interfaces with [Moq](https://github.com/moq/moq4) and acceptance scenarios with [LightBDD](https://github.com/LightBDD/LightBDD)
* Code covering using [Coverlet](https://github.com/tonerdo/coverlet) and [Codecov](https://codecov.io/bb/henriquepolsani/dot-net-boilerplate)
* [CQRS](https://martinfowler.com/bliki/CQRS.html), Commands, Queries & Events handlers using [MediatR](https://github.com/jbogard/MediatR) and [Event Sourcing](https://martinfowler.com/eaaDev/EventSourcing.html)
* Application Performance Management and monitoring with [New Relic](https://newrelic.com/)
* [S3 AWS](https://aws.amazon.com/pt/s3/) to store files dynamically created
* Storing secrets with [Vault](https://www.vaultproject.io/)
* Service discovery with [Consul](https://www.consul.io/)
* Logging with [Serilog](https://serilog.net/), [Datadog](https://www.datadoghq.com/) and [Elastic](https://www.elastic.co/pt/)
* Migrations
* Embeded Application Server [Kestrel](https://docs.microsoft.com/pt-br/aspnet/core/fundamentals/servers/kestrel?view=aspnetcore-2.2)
* Containers [Docker](https://www.docker.com/)
* Resilience with [Polly](https://github.com/App-vNext/Polly)
* [Entity Framework Core](https://github.com/aspnet/EntityFrameworkCore)
* [RabbitMQ](https://www.rabbitmq.com/)

## Endpoints

| Method    | URI                                                 | Description                 | Response Status                          |
| --------- | --------------------------------------------------- | --------------------------- | ---------------------------------------- |
| `GET`     | /healthcheck                                        | Check if service is healthy | `200`, `500`, `503`                      |

## Beblue nuget repository

First of all you need do add Beblue's private nuget feed. You can do it by your IDE (Visual Studio, Rider) or using [Nuget Application](https://www.nuget.org/downloads).

At this time, the current repository address is _**http://develop.beblue.com.br:8081/repository/beblue-nuget/**_.

After added the repository feed in your nuget config, when open your IDE, it will request you an user and password login, to connect to this private repository. If you don't have your personal account, contact **Engineering department**.

## Using Vault

Vault is a open source project to store secrets and encrypt/decrypt data. To use Vault in your .net solution include the `Beblue.Infra.Vault` nuget package from **Beblue nuget repository**.

To consult `Beblue.Infra.Vault` documentation and how to use, please access the [project repository](https://bitbucket.org/beblue/beblue-infra-vault/src/master/) at BitBucket.

## Logging

To log in Datadog in your .net solution include the `Beblue.Infra.Logging` nuget package from **Beblue nuget repository**.

In your `Program.cs` file, when building the application, add the extension for logging, specifying the log minimum level (default is `Information`).

```csharp
WebHost.CreateDefaultBuilder(args)
    .UseStartup<Startup>()
    .UseBeblueLogging(LogLevel.Debug);
```

To consult `Beblue.Infra.Logging` documentation and how to use, please access the [project repository](https://bitbucket.org/beblue/beblue-infra-logging/src/master/) at BitBucket.

## Using AWS S3 Storage

When the application needs to generate an file, for reports by example, we usually use **AWS S3 Service**.

To authenticate on AWS is necessary attribute a _**task role**_ to the container. To do this, add the following line at `ecs-task-definition` file:

```json
{
  "taskRoleArn": "task-role-for-environment"
}
```

In addiction, register the interface `IStorageService` in your _**Dependency Injection Container**_ to resolve this interface with `StorageService` class. To use the storage just send a request like the sample below:

```csharp
public string UploadTestFile(IStorageService service)
{
    using (var ms = new MemoryStream())
    {
        var excel = new ExcelPackage(ms);
        var worksheet = excel.Workbook.Worksheets.Add("Excel Sheet Sample");
        excel.Save();
        
        var downloadLink = service.Upload(ms, "FileName.xlsx", 
            new StorageConfiguration("bucket-name", "path"));
        
        return downloadLink;
    }         
}
```
In addiction to make your code parameterizable, set up the configurations inside `appsettings.json` file, separating by environment.

## Code coverage

To generate code coverage report manually, we need to input the command bellow in project CLI.

```bash
> coverlet 
```

### Maintainer

Henrique Scutti Polsani

henrique.polsani@beblue.com.br