﻿using System;
using System.Collections.Generic;
using System.IO;
using Amazon.Runtime;
using Amazon.S3;
using Amazon.S3.Model;
using Amazon.S3.Transfer;
using Amazon;

namespace DotNetBoilerplate.Infra.Storage
{
    public class StorageService : IStorageService
    {
        private readonly IAmazonS3 _client;

        public StorageService()
        {
            _client = new AmazonS3Client(new ECSTaskCredentials(), RegionEndpoint.USEast2);
        }

        public string Upload(Stream fileStream, string fileName, StorageConfiguration configuration)
        {
            var transferUtility = new TransferUtility(_client);

            fileStream.Position = 0;
            var key = $"{configuration.Path}/{fileName}";

            transferUtility.Upload(new TransferUtilityUploadRequest
            {
                TagSet = new List<Tag> { new Tag { Key = "expire", Value = "true" } },
                InputStream = fileStream,
                BucketName = configuration.BucketName,
                Key = key
            });

            var urlRequest = new GetPreSignedUrlRequest
            {
                BucketName = configuration.BucketName,
                Key = key,
                Expires = DateTime.Now.AddHours(1)
            };

            return _client.GetPreSignedURL(urlRequest);
        }
    }
}
